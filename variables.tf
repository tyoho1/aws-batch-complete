variable "name_prefix" {
  type        = string
  description = "Name for prefix in naming convention of all resources. Default: sample-batch"
  default     = "sample-batch"
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC to launch AWS services into. Defaults to the regions default VPC."
  default     = "default"
}

variable "mandatory_tags" {
  description = "Mapping of tags to add to all taggable resources"
  default     = {}
}

variable "batch_service_policies" {
  type        = list(string)
  description = "List of ARN's of IAM Policies to allow Batch to call additional AWS services. Default: []"
  default     = []
}

variable "compute_environment_type" {
  type        = string
  description = "Type of Batch Compute Environment. 'MANAGED' or 'UNMANAGED'. Default: 'MANAGED'"
  default     = "MANAGED"
}

variable "compute_environment_state" { #######
  type        = string
  description = "State of the Batch Compute Environment. 'ENABLED' or 'DISABLED'. Default: 'ENABLED'"
  default     = "ENABLED"
}

# variable "bid_percentage" { #######
#   type        = number
#   description = "Number representing minimum percentage that a Spot Instance price must be when compared with the On-Demand price. Required for compute_resources_type of 'SPOT'. Default: 20"
#   default     = 20
# }

variable "ec2_key_pair" { #######
  type        = string
  description = "Key Pair name to be associated with instances launched by Batch. Default: null"
  default     = ""
}

variable "instance_policies" {
  type        = list(string)
  description = "List of ARN's of IAM Policies to allow the instance created by Batch to additional AWS services. Default: []"
  default     = []
}

variable "instance_types" {
  type        = list(string)
  description = "List of instance types for the instance created by Batch. Default: ['c4.large']"
  default     = ["c4.large"]
}

variable "max_vcpus" {
  type        = number
  description = "Maximum number of EC2 vCPUs that an environment can reach. Default: 16"
  default     = 16
}

variable "min_vcpus" {
  type        = number
  description = "Minimum number of EC2 vCPUs that an environment can reach. Default: 0"
  default     = 0
}

variable "instance_security_group_ids" {
  type        = list(string)
  description = "List of Security Group IDs for the instance created by Batch. Default: Security Group with open egress."
  default     = []
}

variable "instance_subnets" {
  type        = list(string)
  description = "List of Subnet Ids to launch the instance created by Batch into. Defaults to all subnets associated in the vpc_id variable."
  default     = []
}

variable "compute_resources_type" {
  type        = string
  description = "Type of resource for the Batch Compute Environment 'EC2' or 'SPOT'. Default: 'EC2'"
  default     = "EC2"
}

variable "batch_job_definition_type" {
  type        = string
  description = "Type of Job Definition. Only valid option is 'container'. Default: 'container'"
  default     = "container"
}

variable "job_attempt_duration" {
  type        = number
  description = "The time (in seconds) after which Batch will terminate your job if they have not finished. Default: 9999"
  default     = 9999
}

variable "container_properties" {
  description = "A valid container properties provided as a single valid JSON document. Defaults to a busybox container that runs ls -la"
  default     = "default"
}

variable "job_retry_attemps" {
  type        = number
  description = "Number of times to retry a job. 1 through 10. Default: 1"
  default     = 1
}

variable "job_queue_state" {
  type        = string
  description = "State of the Job Queue. Can be either 'ENABLED' or 'DISABLED'. Default: 'ENABLED'"
  default     = "ENABLED"
}

variable "job_queue_priority" {
  type        = number
  description = "Priority of the job queue. Lower the number higher the priority. Default: 1"
  default     = 1
}
