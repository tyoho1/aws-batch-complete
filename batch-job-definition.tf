resource "aws_batch_job_definition" "main" {
  name                 = "${var.name_prefix}JobDefinition"
  type                 = var.batch_job_definition_type
  container_properties = var.container_properties == "default" ? local.default_container_properties : var.container_properties

  timeout {
    attempt_duration_seconds = var.job_attempt_duration
  }

  retry_strategy {
    attempts = var.job_retry_attemps
  }
}

locals {
  default_container_properties = <<CONTAINER_PROPERTIES
  {
    "command": ["ls", "-la"],
    "image": "busybox",
    "memory": 1024,
    "vcpus": 1,
    "volumes": [
      {
        "host": {
          "sourcePath": "/tmp"
        },
        "name": "tmp"
      }
    ],
    "environment": [
        {"name": "VARNAME", "value": "VARVAL"}
    ],
    "mountPoints": [
        {
          "sourceVolume": "tmp",
          "containerPath": "/tmp",
          "readOnly": false
        }
    ],
    "ulimits": [
      {
        "hardLimit": 1024,
        "name": "nofile",
        "softLimit": 1024
      }
    ]
}
  CONTAINER_PROPERTIES
}
