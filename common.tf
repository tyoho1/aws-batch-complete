data "aws_caller_identity" "current" {}

resource "aws_default_vpc" "default" {}

data "aws_subnet_ids" "default" {
  vpc_id = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id
}
