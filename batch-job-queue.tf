resource "aws_batch_job_queue" "main" {
  name                 = "${var.name_prefix}JobQueue"
  state                = var.job_queue_state
  priority             = var.job_queue_priority
  compute_environments = [aws_batch_compute_environment.main.arn]
}
