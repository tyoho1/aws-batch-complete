# aws-batch-complete

---
A terraform module to create an AWS Batch Compute Environment, Job Definition, and Job Queue. Currently only MANAGED and EC2 is fully supported.

There are no required input variables. You can use this module simply by defining a module block in your TF scripts and adding the appropriate source attribute.
When running this module with no defined input variables it creates a Batch that will load a busybox linux container and run `ls -la`.

## Optional Module Input Variables

---
|Name|Type|Default|Description|
|:---:|:---:|:---:|---|
|name_prefix|string|`"sample-batch"`|Name for prefix in naming convention of all resources.|
|vpc_id|string|Default VPC for the current region|ID of the VPC to launch AWS services into.|
|mandatory_tags|mapping of tags|`{}`|Mapping of tags to add to all taggable resources|
|batch_service_policies|list(string)|`[]`|List of ARN's of IAM Policies to allow Batch to call additional AWS services.|
|compute_environment_type|string|`"MANAGED"`|Type of Batch Compute Environment. 'MANAGED' or 'UNMANAGED'.|
|compute_environment_state|string|`"ENABLED"`|State of the Batch Compute Environment. 'ENABLED' or 'DISABLED'.|
|ec2_key_pair|string| - |Key Pair name to be associated with instances launched by Batch.|
|instance_policies|list(string)|`[]`|List of ARN's of IAM Policies to allow the instance created by Batch to call additional AWS services.|
|instance_types|list(string)|`["c4.large"]`|List of instance types for the instance created by Batch.|
|max_vcpus|number|`16`|Maximum number of EC2 vCPUs that an environment can reach.|
|min_vcpus|number|`0`|Minimum number of EC2 vCPUs that an environment can reach.|
|instance_security_group_ids|list(string)|  `[]`**¹**|List of Security Group IDs for the instance created by Batch.|
|instance_subnets|list(string)|  `[]`**²**|List of Subnet Ids to launch the instance created by Batch into.|
|compute_resources_type|string|`"EC2"`|Type of resource for the Batch Compute Environment 'EC2' or 'SPOT'.|
|batch_job_definition_type|string|`"container"`|Type of Job Definition. Only valid option is 'container'.|
|job_attempt_duration|number|`9999`|The time (in seconds) after which Batch will terminate your job if they have not finished.|
|container_properties|container_properties object. See example|    -**³** |A valid container properties provided as a single valid JSON document.|
|job_retry_attemps|number|`1`|Number of times to retry a job. 1 through 10.|
|job_queue_state|string|`"ENABLED"`|State of the Job Queue. Can be either 'ENABLED' or 'DISABLED'.|
|job_queue_priority|number|`1`|Priority of the job queue. Lower the number higher the priority.|

**¹** By default the instance will have open egress

**²** If not supplied will use all subnets associated with vpc_id

**³** If not supplied will use a busybox container that runs `ls -la`

## Usage

---

### Learning

#### Create a Batch Job that deplpys a busybox container that runs ls -la

```text
module "batch_complete" {
  source = "bitbucket.org/tyoho1/aws-batch-complete"
}
```

### Basic

#### Create Batch Job with some custom settings

```text
module "batch_complete" {
  source = "bitbucket.org/tyoho1/aws-batch-complete"
  name_prefix = "myCustomBatch"
  vpc_id = "vpc-123"
  instance_policies = [aws_iam_role_policy.custom_instance_policy.arn]

  mandatory_tags = {
      Costcenter = "CC12345"
      Environment = "DEV"
  }

  container_properties = <<CONTAINER_PROPERTIES
  {
    "command": ["ls", "-la"],
    "image": "busybox",
    "memory": 1024,
    "vcpus": 1,
    "volumes": [
      {
        "host": {
          "sourcePath": "/tmp"
        },
        "name": "tmp"
      }
    ],
    "environment": [
        {"name": "VARNAME", "value": "VARVAL"}
    ],
    "mountPoints": [
        {
          "sourceVolume": "tmp",
          "containerPath": "/tmp",
          "readOnly": false
        }
    ],
    "ulimits": [
      {
        "hardLimit": 1024,
        "name": "nofile",
        "softLimit": 1024
      }
    ]
}
  CONTAINER_PROPERTIES
}
```

### Advanced

#### Create Batch Job that runs a custom script and pulls an image from ECR. The container uses a secret environment variable that is supplied by SSM Parameter Store

```text
module "batch_complete" {
  source = "bitbucket.org/tyoho1/aws-batch-complete"
  name_prefix = "myCustomScript"
  vpc_id = "vpc-123"
  batch_service_role = [aws_iam_role_policy.custom_service_policy.arn]
  compute_environment_type = "MANAGED"
  compute_environment_state = "ENABLED"
  ec2_key_pair = "my_key_pair"
  instance_policies = [aws_iam_role_policy.custom_instance_policy.arn]
  instance_types = ["c4", "c5"]
  max_vcpus = 32
  min_vcpus = 8
  instance_security_group_ids = ["sg-123", "sg-456"]
  instance_subnets = ["subnet-321", "subnet-654"]
  compute_resources_type = "EC2"
  batch_job_definition_type = "container"
  job_attempt_duration = 600
  job_retry_attemps = 2
  job_queue_state = "ENABLED"
  job_queue_priority = 1

  mandatory_tags = {
      Costcenter = "CC12345"
      Environment = "DEV"
  }

  container_properties = <<CONTAINER_PROPERTIES
  {
    "command": ["./custom_script.sh"],
    "image": "987654321.dkr.ecr.us-east-1.amazonaws.com/myCustomImage:latest",
    "memory": 1024,
    "vcpus": 8,
    "volumes": [
      {
        "host": {
          "sourcePath": "/tmp"
        },
        "name": "tmp"
      }
    ],
    "environment": [
        {"name": "SECRET_PASSWORD_FOR_SCRIPT", "value": aws_ssm_parameter.secret_password.value}
    ],
    "mountPoints": [
        {
          "sourceVolume": "tmp",
          "containerPath": "/tmp",
          "readOnly": false
        }
    ],
    "ulimits": [
      {
        "hardLimit": 1024,
        "name": "nofile",
        "softLimit": 1024
      }
    ]
}
  CONTAINER_PROPERTIES
}
```

## Outputs

---

- `batch_compute_environment_arn` - ARN of the Compute Environment
- `batch_compute_environment_status` - Status of the Compute Environment
- `batch_compute_environment_status_reason` - Reason for the current status of the Compute Environment
- `batch_compute_environment_ecs_cluster_arn` - ARN of the underlying ECS Cluster utilized by the Compute Environment
- `batch_job_queue_arn` - ARN of the Batch Job Queue
- `batch_job_definition_arn` - ARN of the Batch Job Definition
- `batch_job_definition_revision` - Revision of the Batch Job Definition

## Authors

---
<tyoho@hgsdigital.com>
