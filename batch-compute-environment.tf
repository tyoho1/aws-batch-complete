resource "aws_batch_compute_environment" "main" {
  compute_environment_name = "${var.name_prefix}ComputeEnvironment"
  service_role             = aws_iam_role.batch_service_role.arn
  type                     = var.compute_environment_type
  state                    = var.compute_environment_state

  compute_resources {
    instance_role      = aws_iam_instance_profile.batch_instance_profile.arn
    instance_type      = var.instance_types
    max_vcpus          = var.max_vcpus
    min_vcpus          = var.min_vcpus
    security_group_ids = concat([aws_security_group.instance_sg.id], var.instance_security_group_ids)
    subnets            = length(var.instance_subnets) == 0 ? data.aws_subnet_ids.default.ids : var.instance_subnets
    type               = var.compute_resources_type
    ec2_key_pair       = var.ec2_key_pair == "" ? null : var.ec2_key_pair
    ## SPOT ATTRIBUTES ##
    # bid_percentage = var.compute_resources_type == "SPOT" ? var.bid_percentage : null

    tags = merge(
      {
        Name = "${var.name_prefix}Instance"
      },
      var.mandatory_tags
    )
  }

  depends_on = [aws_iam_role_policy_attachment.batch_service_main_policy]
}

resource "aws_iam_role" "batch_instance_role" {
  name = "${var.name_prefix}InstanceRole"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
        "Service": "ec2.amazonaws.com"
        }
    }
    ]
}
EOF

  tags = var.mandatory_tags
}

resource "aws_iam_role_policy_attachment" "ec2_container_main_policy" {
  role       = aws_iam_role.batch_instance_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ec2_container_extra_policy" {
  count = length(var.instance_policies)

  role       = aws_iam_role.batch_instance_role.name
  policy_arn = var.instance_policies[count.index]
}

resource "aws_iam_instance_profile" "batch_instance_profile" {
  name = "${var.name_prefix}InstanceProfile"
  role = aws_iam_role.batch_instance_role.name

  depends_on = [aws_iam_role_policy_attachment.ec2_container_main_policy]
}

resource "aws_iam_role" "batch_service_role" {
  name = "${var.name_prefix}ServiceRole"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Effect": "Allow",
        "Principal": {
        "Service": "batch.amazonaws.com"
        }
    }
    ]
}
EOF

  tags = var.mandatory_tags
}

resource "aws_iam_role_policy_attachment" "batch_service_main_policy" {
  role       = aws_iam_role.batch_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole"
}

resource "aws_iam_role_policy_attachment" "batch_service_extra_policy" {
  count = length(var.batch_service_policies)

  role       = aws_iam_role.batch_service_role.name
  policy_arn = var.batch_service_policies[count.index]
}

# resource "aws_iam_role_policy" "batch_instance_role_policy" {
#   count = var.instance_role == "default" ? 1 : 0

#   name = "${var.name_prefix}InstanceRolePolicy"
#   role = aws_iam_role.batch_instance_role[count.index].name

#   policy = <<-EOF
#   {
#     "Version": "2012-10-17",
#     "Statement": [
#       {
#         "Effect": "Allow",
#         "Action": [
#           "ecr:GetAuthorizationToken",
#           "ecr:BatchCheckLayerAvailability",
#           "ecr:GetDownloadUrlForLayer",
#           "ecr:GetRepositoryPolicy",
#           "ecr:DescribeRepositories",
#           "ecr:ListImages",
#           "ecr:DescribeImages",
#           "ecr:BatchGetImage",
#           "ecr:GetLifecyclePolicy",
#           "ecr:GetLifecyclePolicyPreview",
#           "ecr:ListTagsForResource",
#           "ecr:DescribeImageScanFindings"
#         ],
#         "Resource": "*"
#       }
#     ]
#   }
#   EOF
# }

resource "aws_security_group" "instance_sg" {
  name        = "${var.name_prefix}InstanceSG"
  description = "Only allow outbound traffic"
  vpc_id      = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.mandatory_tags
}
