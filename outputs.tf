output "batch_compute_environment_arn" {
  description = "ARN of the Batch Compute Environment"
  value       = aws_batch_compute_environment.main.arn
}

output "batch_compute_environment_status" {
  description = "Status of the Batch Compute Environment"
  value       = aws_batch_compute_environment.main.status
}

output "batch_compute_environment_status_reason" {
  description = "Reason for the current status of the Batch Compute Environment"
  value       = aws_batch_compute_environment.main.status_reason
}

output "batch_compute_environment_ecs_cluster_arn" {
  description = "ARN of the underlying ECS Cluster utilized by the Compute Environment"
  value       = aws_batch_compute_environment.main.ecs_cluster_arn
}

output "batch_job_queue_arn" {
  description = "ARN of the Batch Job Queue"
  value       = aws_batch_job_queue.main.arn
}

output "batch_job_definition_arn" {
  description = "ARN of the Batch Job Definition"
  value       = aws_batch_job_definition.main.arn
}

output "batch_job_definition_revision" {
  description = "Revision of the Batch Job Definition"
  value       = aws_batch_job_definition.main.revision
}
